# Docker-Only-Office-Chinese-font

## 介绍

本项目是 Only Office Docker 镜像的延伸，主要是为 Only Office 添加 中文字体。

## 使用方法

```
git clone https://gitee.com/zhuhaos/onlyoffice.git
cd Docker-Only-Office-Chinese-font
docker build -t onlyoffice/chinese .
sudo docker run -i -t -d -p 82:80 onlyoffice/chinese
```

待镜像构建完毕后，即可使用。


